package com.wmt.topicvoter.ui.topiclist;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.wmt.topicvoter.R;
import com.wmt.topicvoter.ui.base.OnRVItemClickListener;
import com.wmt.topicvoter.ui.model.TopicModel;
import com.wmt.topicvoter.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class TopicPresenter implements OnRVItemClickListener {
    private TopicView topicView;
    private Context mContext;
    private TopicAdapter topicAdapter;
    int maxUpDownVote = 50;
    String[] topicsList;
    private Random rand;

    TopicPresenter(TopicView topicView, Context context) {
        this.topicView = topicView;
        this.mContext = context;
        topicsList = mContext.getResources().getStringArray(R.array.array_topics);
        rand = new Random();
    }

    /********************************
     * set prefix data
     * *******************************/
    void setTopicData(RecyclerView recyclerView) {
        topicAdapter = new TopicAdapter(TopicViewHolder.class, R.layout.topic_raw, this, recyclerView);
        topicAdapter.setItems(getTopics());
        topicView.setDrawerAdapter(topicAdapter);
    }

    private List<TopicModel> getTopics() {
        List<TopicModel> topicModelList = new ArrayList<>();

        for (int i = 0; i < topicsList.length; i++) {
            TopicModel model = new TopicModel();
            model.setTopicId(i);
            model.setTopicUpVote(rand.nextInt(maxUpDownVote));
            model.setTopicDownVote(rand.nextInt(maxUpDownVote));
            model.setTopicTitle(topicsList[i]);
            topicModelList.add(model);
        }
        Collections.sort(
                topicModelList,
                (lhs, rhs) -> {
                    if (lhs.getTopicUpVote() > rhs.getTopicUpVote()) {
                        return -1;
                    } else if (lhs.getTopicUpVote() < rhs.getTopicUpVote()) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
        );
        return topicModelList;
    }

    /********************************
     *sort topics by up-votes
     * *******************************/
    private void sortListByVote(List<TopicModel> topicModelList) {
        Collections.sort(
                topicModelList,
                (lhs, rhs) -> {
                    if (lhs.getTopicUpVote() > rhs.getTopicUpVote()) {
                        return -1;
                    } else if (lhs.getTopicUpVote() < rhs.getTopicUpVote()) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
        );
        topicAdapter.setItems(topicModelList);
    }

    /********************************
     *sort topics by down-votes
     * *******************************/
    private void sortByRecently(List<TopicModel> topicModelList) {
        Collections.sort(
                topicModelList,
                (lhs, rhs) -> {
                    if (lhs.getTopicUpVote() > rhs.getTopicUpVote()) {
                        return 1;
                    } else if (lhs.getTopicUpVote() < rhs.getTopicUpVote()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
        );
        topicAdapter.setItems(topicModelList);
    }

    @Override
    public void onItemClick(View v, int position) {
        topicView.recItemClick(v, position);
    }

    /********************************
     *Add upvote to topic
     * *******************************/
    void upVote(int position) {
        TopicModel item = topicAdapter.getItem(position);
        item.setTopicUpVote(item.getTopicUpVote() + 1);
        switch (topicAdapter.getAdapterType()) {
            case Constants.ALL_TOPICS:
                topicAdapter.updateItem(item, position);
                break;
            case Constants.TOP_20_TOPICS:
                sortListByVote(topicAdapter.getItems());
                break;
        }
    }

    /********************************
     *Add down vote to topic
     * *******************************/
    void downVote(int position) {
        TopicModel item = topicAdapter.getItem(position);
        item.setTopicDownVote(item.getTopicDownVote() + 1);
        switch (topicAdapter.getAdapterType()) {
            case Constants.ALL_TOPICS:
                topicAdapter.updateItem(item, position);
                break;
            case Constants.TOP_20_TOPICS:
                sortListByVote(topicAdapter.getItems());
                break;
        }

    }

    /********************************
     *Add topic in topic list
     * *******************************/
    void addTopic(String topicContent) {
        List<TopicModel> items = topicAdapter.getItems();
        TopicModel model = new TopicModel();
        model.setTopicId(items.size() + 1);
        model.setTopicUpVote(0);
        model.setTopicDownVote(0);
        model.setTopicTitle(topicContent);
        items.add(model);
        sortListByVote(items);
    }

    /********************************
     *Display all topics and trandding topics
     * *******************************/
    void displayAll(AppCompatImageView appCompatImageView, AppCompatTextView appCompatTextView) {
        switch (topicAdapter.getAdapterType()) {
            case Constants.ALL_TOPICS:
                topicAdapter.displayLength(Constants.TOP_20_TOPICS);
                appCompatImageView.setImageResource(R.drawable.ic_top_20);
                sortListByVote(topicAdapter.getItems());
                appCompatTextView.setText(mContext.getString(R.string.trending_tpoics));
                break;
            case Constants.TOP_20_TOPICS:
                topicAdapter.displayLength(Constants.ALL_TOPICS);
                appCompatImageView.setImageResource(R.drawable.ic_display_all);
                sortByRecently(topicAdapter.getItems());
                appCompatTextView.setText(R.string.all_topics);
                break;
        }

    }
}
