package com.wmt.topicvoter.ui.addtopic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.wmt.topicvoter.R;
import com.wmt.topicvoter.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddTopicActivity extends AppCompatActivity implements AddTopicView {


    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.edtTopicContent)
    AppCompatEditText edtTopicContent;
    @BindView(R.id.btnAddTopic)
    AppCompatButton btnAddTopic;
    private AddTopicPresenter addTopicPresenter;
    @BindView(R.id.imgAdd)
    AppCompatImageView imgAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_topic);
        ButterKnife.bind(this);
        addTopicPresenter = new AddTopicPresenter(AddTopicActivity.this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgAdd.setVisibility(View.GONE);
        tvTitle.setText(R.string.add_topic);
    }

    @OnClick({R.id.btnAddTopic, R.id.imgBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnAddTopic:
                addTopicPresenter.addTopic(edtTopicContent.getText().toString().trim());
                break;
        }
    }

    /********************************
     *Return error from presnter
     * *******************************/
    @Override
    public void showError(String errorMessage) {
        Utils.showSnackBar(btnAddTopic, errorMessage);
    }

    /********************************
     * provide recyclerview click  to activtiy
     * *******************************/
    @Override
    public void addTopic(String topic) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("topic_content", topic);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
