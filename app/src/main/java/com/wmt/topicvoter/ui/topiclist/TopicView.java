package com.wmt.topicvoter.ui.topiclist;

import android.view.View;

import com.wmt.topicvoter.ui.base.BaseRecyclerAdapter;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public interface TopicView {

    void setDrawerAdapter(BaseRecyclerAdapter baseRecyclerAdapter);


    void recItemClick(View view, int postion);

}
