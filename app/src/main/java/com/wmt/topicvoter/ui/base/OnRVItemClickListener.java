package com.wmt.topicvoter.ui.base;

import android.view.View;

public interface OnRVItemClickListener {
    void onItemClick(View v, int position);
}
