package com.wmt.topicvoter.ui.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wmt.topicvoter.R;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private Class<T> mModelClass;
    private int mModelLayout;
    private Class<VH> mViewHolderClass;
    private List<T> mList = new ArrayList<>();
    private OnRVItemClickListener onRVItemClickListener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading = true;
    private OnLoadMoreListener onLoadMoreListener;

    public BaseRecyclerAdapter(Class<VH> viewHolderClass, int modelLayout, OnRVItemClickListener onRVItemClickListener, RecyclerView recyclerView) {
        mModelLayout = modelLayout;
        mViewHolderClass = viewHolderClass;
        this.onRVItemClickListener = onRVItemClickListener;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    /********************************
     *Get item value at postion
     * *******************************/
    public T getItem(int position) {
        return mList.get(position);
    }

    /********************************
     *Get list to adapter
     * *******************************/
    public List<T> getItems() {
        return mList;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(mModelLayout, parent, false);
            try {
                Constructor<VH> constructor = mViewHolderClass.getConstructor(View.class, OnRVItemClickListener.class);
                return constructor.newInstance(view, onRVItemClickListener);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        } else {
            ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_progress, parent, false);
            try {
                Class<LoadMoreViewHolder> loadMoreViewHolderClass = LoadMoreViewHolder.class;
                Constructor<VH> constructor = (Constructor<VH>) loadMoreViewHolderClass.getConstructor(View.class, OnRVItemClickListener.class);
                return constructor.newInstance(view, onRVItemClickListener);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull VH viewHolder, int position) {
        T model = getItem(position);
        populateViewHolder(viewHolder, model, position);
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    abstract protected void populateViewHolder(VH viewHolder, T model, int position);

    /********************************
     *Add item  to adapter
     * *******************************/
    public void addItem(T t) {
        int index = mList.size();
        mList.add(index, t);
        notifyItemInserted(index);
    }

    /********************************
     *Update item  to adapter at specific posotion
     * *******************************/
    public void updateItem(T t, int position) {
        mList.set(position, t);
        notifyItemChanged(position);
    }

    /********************************
     *Remove item  to adapter at specific posotion
     * *******************************/
    public void removeItem(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

    /********************************
     *Set list to adapter
     * *******************************/
    public void setItems(List<T> items) {
        mList = items;
        notifyDataSetChanged();
    }

    /********************************
     *Clear adapter
     * *******************************/
    public void clearItems() {
        mList.clear();
        notifyDataSetChanged();
    }

}