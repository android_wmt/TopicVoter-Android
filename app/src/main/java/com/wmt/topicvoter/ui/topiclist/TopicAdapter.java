package com.wmt.topicvoter.ui.topiclist;

import android.support.v7.widget.RecyclerView;

import com.wmt.topicvoter.ui.base.BaseRecyclerAdapter;
import com.wmt.topicvoter.ui.base.OnRVItemClickListener;
import com.wmt.topicvoter.ui.model.TopicModel;
import com.wmt.topicvoter.utils.Constants;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class TopicAdapter extends BaseRecyclerAdapter<TopicModel, TopicViewHolder> {

    private int type;

    TopicAdapter(Class<TopicViewHolder> viewHolderClass, int modelLayout, OnRVItemClickListener onRVItemClickListener, RecyclerView recyclerView) {
        super(viewHolderClass, modelLayout, onRVItemClickListener, recyclerView);
        this.type = Constants.TOP_20_TOPICS;
    }

    @Override
    protected void populateViewHolder(TopicViewHolder viewHolder, TopicModel model, int position) {
        viewHolder.getTvContent().setText(model.getTopicTitle());
        viewHolder.getTvUpVote().setText(String.valueOf(model.getTopicUpVote()));
        viewHolder.getTvDownVote().setText(String.valueOf(model.getTopicDownVote()));
    }

    void displayLength(int type) {
        this.type = type;
    }

    int getAdapterType() {
        return type;
    }

    @Override
    public int getItemCount() {
        int length = 0;
        switch (type) {
            case Constants.ALL_TOPICS:
                length = super.getItemCount();
                break;
            case Constants.TOP_20_TOPICS:
                if (super.getItemCount() > 20)
                    length = 20;
                else
                    length = super.getItemCount();
                break;
        }
        return length;
    }
}
