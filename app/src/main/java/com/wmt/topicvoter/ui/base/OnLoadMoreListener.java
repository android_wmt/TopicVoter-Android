package com.wmt.topicvoter.ui.base;

public interface OnLoadMoreListener {
    void onLoadMore();
}