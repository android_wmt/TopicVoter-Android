package com.wmt.topicvoter.ui.base;

import android.view.View;
import android.widget.ProgressBar;


import com.wmt.topicvoter.R;

import butterknife.BindView;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 2/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class LoadMoreViewHolder extends BaseViewHolder {
    @BindView(R.id.prLoadMore)
    ProgressBar prLoadMore;

    public LoadMoreViewHolder(View itemView, OnRVItemClickListener rvItemClickListener) {
        super(itemView, rvItemClickListener);
    }
}
