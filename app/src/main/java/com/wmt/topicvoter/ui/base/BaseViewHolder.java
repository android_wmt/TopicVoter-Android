package com.wmt.topicvoter.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private OnRVItemClickListener onRVItemClickListener;

    public BaseViewHolder(View itemView, OnRVItemClickListener rvItemClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.onRVItemClickListener = rvItemClickListener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onRVItemClickListener.onItemClick(v, getAdapterPosition());
    }
}