package com.wmt.topicvoter.ui.model;

import lombok.Data;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
@Data
public class TopicModel {
    private String topicTitle;
    private int topicUpVote, topicDownVote, topicId;
}
