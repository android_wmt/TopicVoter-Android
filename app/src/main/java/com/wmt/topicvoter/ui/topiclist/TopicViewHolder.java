package com.wmt.topicvoter.ui.topiclist;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.wmt.topicvoter.R;
import com.wmt.topicvoter.ui.base.BaseViewHolder;
import com.wmt.topicvoter.ui.base.OnRVItemClickListener;

import butterknife.BindView;


/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class TopicViewHolder extends BaseViewHolder {
    @BindView(R.id.tvUpVote)
    AppCompatTextView tvUpVote;
    @BindView(R.id.tvDownVote)
    AppCompatTextView tvDownVote;
    @BindView(R.id.tvContent)
    AppCompatTextView tvContent;

    AppCompatTextView getTvUpVote() {
        return tvUpVote;
    }


    AppCompatTextView getTvDownVote() {
        return tvDownVote;
    }


    AppCompatTextView getTvContent() {
        return tvContent;
    }


    public TopicViewHolder(View itemView, OnRVItemClickListener rvItemClickListener) {
        super(itemView, rvItemClickListener);
        tvUpVote.setOnClickListener(this);
        tvDownVote.setOnClickListener(this);
    }
}
