package com.wmt.topicvoter.ui.addtopic;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.wmt.topicvoter.R;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 19/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class AddTopicPresenter {
    private Context mContext;
    private AddTopicView addTopicView;

    AddTopicPresenter(Context mContext, AddTopicView addTopicView) {
        this.mContext = mContext;
        this.addTopicView = addTopicView;
    }

    /********************************
     *Get data from Activity and validate data
     * *******************************/
    void addTopic(String topicContent) {
        if (!TextUtils.isEmpty(topicContent)) {
            addTopicView.addTopic(topicContent);
        } else {
            addTopicView.showError(mContext.getString(R.string.error_add_topic_content));
        }
    }
}
