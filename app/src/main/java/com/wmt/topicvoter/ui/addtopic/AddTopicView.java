package com.wmt.topicvoter.ui.addtopic;

import android.os.Bundle;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 19/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public interface AddTopicView {

    void showError(String errorMessage);

    void addTopic(String topic);
}
