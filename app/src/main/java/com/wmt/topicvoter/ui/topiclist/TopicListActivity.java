package com.wmt.topicvoter.ui.topiclist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.wmt.topicvoter.R;
import com.wmt.topicvoter.ui.addtopic.AddTopicActivity;
import com.wmt.topicvoter.ui.base.BaseRecyclerAdapter;
import com.wmt.topicvoter.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TopicListActivity extends AppCompatActivity implements TopicView {

    @BindView(R.id.recTopics)
    RecyclerView recTopics;
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    private TopicPresenter topicPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        topicPresenter = new TopicPresenter(this, TopicListActivity.this);
        topicPresenter.setTopicData(recTopics);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvTitle.setText(R.string.trending_tpoics);
        imgBack.setImageResource(R.drawable.ic_display_all);
    }
    /********************************
     * provide aapter to Recyclerview from presenter
     * *******************************/
    @Override
    public void setDrawerAdapter(BaseRecyclerAdapter baseRecyclerAdapter) {
        recTopics.setAdapter(baseRecyclerAdapter);
    }

    @OnClick({R.id.imgAdd, R.id.imgBack})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgAdd:
                Intent addTopicIntent = new Intent(TopicListActivity.this, AddTopicActivity.class);
                startActivityForResult(addTopicIntent, Constants.ADD_TOPIC);
                break;
            case R.id.imgBack:
                topicPresenter.displayAll(imgBack,tvTitle);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null)
            switch (requestCode) {
                case Constants.ADD_TOPIC:
                    topicPresenter.addTopic(data.getStringExtra("topic_content"));
                    break;
            }
    }
    /********************************
     * provide aapter to Recyclerview from presenter
     * *******************************/
    @Override
    public void recItemClick(View view, int postion) {
        switch (view.getId()) {
            case R.id.tvUpVote:
                topicPresenter.upVote(postion);
                break;
            case R.id.tvDownVote:
                topicPresenter.downVote(postion);
                break;
        }
    }
}
