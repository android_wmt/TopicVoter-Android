package com.wmt.topicvoter.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 19/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class Utils {
    /********************************
     * For display any error message or any message to user
     * *******************************/
    public static void showSnackBar(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }
}
