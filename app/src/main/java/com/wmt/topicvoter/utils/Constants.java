package com.wmt.topicvoter.utils;

/**
 * @author Milan Pansuriya
 *         for WebMob Technologies.
 *         Email:android@webmobtech.com.
 * @since 18/4/18.
 * Copyright (c) 2018 WebMob Technologies.
 */
public class Constants {
    public static final int ADD_TOPIC = 1001;
    public static final int TOP_20_TOPICS = 1002;
    public static final int ALL_TOPICS = 1003;
}
